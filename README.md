# Introduction

The goal of this tool is to allow the system adiminstrator to add a
list of arguments to any "java.exe" command executed. 

# Installation

- To install the program simply execute: install.exe

The install script will show you where to find the file to edit in order to add the arguments.

- Do the same to uninstall, execute: uninstall.exe


# Internals

To do this, the following steps are taken. 

1) The original "java.exe" command replaced by our command "java\_wrapper.exe":
   a) rename the default java.exe to java\_default.exe
   b) java\_wrapper.exe is renamed java.exe and placed at the location of java.exe

2) Our java\_wrapper.exe will simply call java\_default.exe with the right
   commands.  To do that it will first read from  a file a list of arguments the
   append the default argument to it.

# Implementation

All three scripts are written powershell. Although, this later can be directly
executed, it cannot be done easily. This is why we want to convert the scripts
from powershell (files with "ps1" extension) to executables (files with ".exe"
extension).

There are many tools that can generate executables from powershell scripts. 
PS2EXE-GUI is one of them. 


# Known Bugs

There seems to be an issue with PS2EXE-GUI. The genrated ".exe" does not behave as
the ".ps". In particular the arguments gets crumbled when the ".exe" is executed.
For example when exceuting "java -version", the actual command is "java_default 
verstion true". The expected command should have been "java_default -verstion".

Potential Solution: we may want to use tools other than the PS2EXE-GUI.
