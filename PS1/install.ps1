#! /usr/bin/pwsh


$JAVA_WRAPPER_ARGS="java_wrapper_args.txt"

$JAVA_WRAPPER="java_wrapper.exe"

$JAVA_BACKUP_NAME="java_default.exe"



$JAVA_CMD=(Get-Command java | Select-Object -ExpandProperty Definition | select -first 1)
$JAVA_DIR=(Split-Path -Path $JAVA_CMD)
$JAVA_NAME=(Split-Path -Path $JAVA_CMD -Leaf -Resolve)
$JAVA_BACKUP_CMD=(Join-Path -Path $JAVA_DIR -ChildPath $JAVA_BACKUP_NAME)


if((Test-Path  $JAVA_BACKUP_CMD)){
    Write-Host "Already installed $JAVA_BACKUP_CMD"
    Exit
}

#Backup the original command
Move-Item -Path $JAVA_CMD -Destination $JAVA_BACKUP_CMD

#Install the wrapper script
Copy-Item -Force $JAVA_WRAPPER -Destination $JAVA_CMD

#Update the path variable in the wrapper
$JAVA_ARGS_PATH=(Join-Path -Path $JAVA_DIR -ChildPath $JAVA_WRAPPER_ARGS)
if(-Not (Test-Path  $JAVA_ARGS_PATH)){
    Copy-Item -Force $JAVA_WRAPPER_ARGS -Destination $JAVA_DIR
    Exit
}
Write-Host "You will find $JAVA_WRAPPER_ARGS in this folder: $JAVA_DIR"

