#! /usr/bin/pwsh


$JAVA_WRAPPER_ARGS="java_wrapper_args.txt"

$JAVA_WRAPPER="java_wrapper.exe"

$JAVA_BACKUP_NAME="java_default.exe"


#Path to current folder
$JAVA_CMD=(Get-Command java | Select-Object -ExpandProperty Definition | select -first 1)
$JAVA_DIR=(Split-Path -Path $JAVA_CMD)
$JAVA_NAME=(Split-Path -Path $JAVA_CMD -Leaf -Resolve)
$JAVA_BACKUP_CMD=(Join-Path -Path $JAVA_DIR -ChildPath $JAVA_BACKUP_NAME)


if(-Not (Test-Path  $JAVA_BACKUP_CMD)){
    Write-Host "Nothing to uninstall"
    Exit
}

#Backup the original command
Move-Item -Force -Path $JAVA_BACKUP_CMD -Destination $JAVA_CMD