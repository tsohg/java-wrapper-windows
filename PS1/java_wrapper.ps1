#! /usr/bin/pwsh

$JAVA_WRAPPER_ARGS="java_wrapper_args.txt"

$JAVA_WRAPPER="java_wrapper.exe"

$JAVA_BACKUP_NAME="java_default.exe"



#Get arguments
$JAVA_CMD=(Get-Command java_default | Select-Object -ExpandProperty Definition | select -first 1)
$JAVA_DIR=(Split-Path -Path $JAVA_CMD)
$ADDED_ARGS_FILE=(Join-Path -Path $JAVA_DIR -ChildPath $JAVA_WRAPPER_ARGS)
$ADDED_ARGS=(Get-Content $ADDED_ARGS_FILE)
 

#For debug only
Write-Host "Actual command executed java  $ADDED_ARGS $args"

#Execute the command
$JAVA_BACKUP_CMD=(Get-Command $JAVA_BACKUP_NAME | Select-Object -ExpandProperty Definition | select -first 1)
Start-Process -NoNewWindow -FilePath "$JAVA_BACKUP_CMD" -ArgumentList "$ADDED_ARGS $args"